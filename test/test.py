import unittest
from app import sumar
from app import restar
from app import mult
from app import div

class sumatest(unittest.TestCase):
    def test_sumar(self):
        self.assertEqual(sumar(2,2),4)

    def test_restar(self):
        self.assertEqual(restar(2,2),0)

    def test_mult(self):
        self.assertEqual(mult(2,3),6)

    def test_div(self):
        self.assertEqual(div(2,2),1)

if __name__ == '__main__':
    unittest.main()